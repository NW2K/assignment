﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{
    /// <summary>
    /// Drawing class for drawing a filled circle.
    /// </summary>
    class CircleFill : DrawingCommand
    {
        /// <summary>
        /// The radius of the circle.
        /// </summary>
        public int Radius { get; private set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CircleFill()
        {
            SetPosition(0, 0);
            Radius = 0;
        }

        /// <summary>
        /// Constructor where the x, y and radius values are put in.
        /// </summary>
        /// <param name="x">The x value of the point where the circle will be drawn.</param>
        /// <param name="y">The y value of the point where the circle will be drawn.</param>
        /// <param name="radius">The radius of the circle.</param>
        public CircleFill(int x, int y, int radius)
        {
            SetPosition(x, y);
            Radius = radius;
        }

        /// <summary>
        /// Method used to change the x, y and radius values after construction.
        /// </summary>
        /// <param name="x">The x value of the point where the circle will be drawn.</param>
        /// <param name="y">The y value of the point where the circle will be drawn.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <param name="dummy">Dummy variable.</param>
        /// <param name="dummy2">Dummy variable.</param>
        public override void SetValues(int x, int y, int radius, int dummy, int dummy2)
        {
            SetPosition(x, y);
            Radius = radius;
        }

        /// <summary>
        /// Runs the program and draws a filled circle.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        public override void RunCommand(Bitmap myBitmap)
        {
            SolidBrush myBrush = new SolidBrush(Color.Black);

            Graphics g = Graphics.FromImage(myBitmap);
            g.FillEllipse(myBrush, X, Y, Radius, Radius);
            myBrush.Dispose();
        }
    }
}
