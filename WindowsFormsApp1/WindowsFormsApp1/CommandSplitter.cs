﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace DrawingProgram
{
    /// <summary>
    /// Class that takes in the command or commands from CommandParser, and utilises various classes to get
    /// a runnable program and runs it through to CommandRunner.
    /// </summary>
    public class CommandSplitter
    {
        /// <summary>
        /// The main CommandRunner class.
        /// </summary>
        private CommandRunner commandRunner = new CommandRunner();

        /// <summary>
        /// The main UserVariables class.
        /// </summary>
        private UserVariables userVariables = new UserVariables();
        /// <summary>
        /// The main UserMethods class.
        /// </summary>
        private UserMethods methods = new UserMethods();

        /// <summary>
        /// A List of UserVariables associated with each created method.
        /// </summary>
        public List<UserVariables> MethodVariables { get; private set; } = new List<UserVariables>();

        /// <summary>
        /// The command that is split using one of the seperators.
        /// </summary>
        public String[] SplitCommand { get; private set; }

        /// <summary>
        /// Parameters to be sent to CommandRunner.
        /// </summary>
        public int[] Parameters { get; private set; }

        /// <summary>
        /// An array of command flags assigned to each command line.
        /// These flags are later used to determine what type of command each line is.
        /// If the command flag is false, then the command will not be run as it is in a bracket.
        /// </summary>
        public String[] CommandTypes { get; private set; }
        /// <summary>
        /// A command flag used in CheckCommand
        /// Used to determine command type in a per line basis.
        /// </summary>
        public String CommandType { get; private set; } = "";
        /// <summary>
        /// String array of command parameters to be passed into UserVariables,
        /// UserMethods or CommandFactory.
        /// </summary>
        public String[] CommandParams { get; private set; }

        /// <summary>
        /// Seperates spaces in a command.
        /// Used to seperate the command type from the parameters
        /// </summary>
        private readonly String[] seperator1 = { " " };
        /// <summary>
        /// Seperates spaces and commas in a command.
        /// Used to seperate parameters.
        /// </summary>
        private readonly String[] seperator2 = { ",", " " };
        /// <summary>
        /// Seperates spaces and equal operators in a command.
        /// Used to seperate the command when it is creating a variable.
        /// </summary>
        private readonly String[] seperator3 = { "=", " " };
        /// <summary>
        /// Seperates spaces, commas and brackets in a command.
        /// Used to seperate the command when it is creating or calling a method.
        /// </summary>
        private readonly String[] seperator4 = { "(", " ", ")", "," };
        /// <summary>
        /// Seperates spaces and various operators in a command.
        /// Used to seperate the command when it is editting a variable.
        /// </summary>
        private readonly String[] seperator5 = { " ", "+", "-", "=" };

        /// <summary>
        /// Maximum number of parameters to be used in the program.
        /// </summary>
        private readonly int maximumParams = 3;

            

        /// <summary>
        /// Default constructor.
        /// Initialises maximum number of parameters.
        /// </summary>
        public CommandSplitter()
        {
            Parameters = new int[maximumParams];
        }

        /// <summary>
        /// Method that runs commands that only require a single line of code.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        /// <param name="command">A single line command passed in.</param>
        /// <exception cref="SyntaxException">
        /// Thrown when CommandType is not equal to any of the cases, resulting in a syntax error.
        /// Also thrown when IndexOutOfRange is thrown.
        /// </exception>
        public void RunCommand(Bitmap myBitmap, String command)
        {
            String[] ThisCommand = command.Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                switch (CommandType)
                {
                    case "Basic":
                        // Three parameters are input here, change if there are more.
                        commandRunner.RunBasicCommand(myBitmap, ThisCommand[0], Parameters[0], Parameters[1], Parameters[2]);
                        break;
                    case "Single":
                        commandRunner.RunSingleCommand(myBitmap, ThisCommand[0]);
                        break;
                    case "CreateVar":
                        ThisCommand = ThisCommand[1].Split(seperator3, 2, StringSplitOptions.RemoveEmptyEntries);
                        if (ThisCommand.Length == 2)
                        {
                            userVariables.CreateVariable(ThisCommand[0], Parameters[0]);
                        }
                        // If no integer has been input, initialise as 0.
                        if (ThisCommand.Length == 1)
                        {
                            userVariables.CreateVariable(ThisCommand[0], 0);
                        }
                        break;
                    case "EditVar":
                        ThisCommand = command.Split(seperator1, 3, StringSplitOptions.RemoveEmptyEntries);
                        userVariables.EditVariable(ThisCommand[0], ThisCommand[1], Parameters[0]);
                        break;
                    case "CallMethod":
                        // Add stuff here.
                        //if (variables.CheckVariable(ThisCommand[0]))
                        //RunCommands(myBitmap, variables.GetMethod(ThisCommand[0]),); // How to use it?
                        ThisCommand = ThisCommand[1].Split(seperator3, 2, StringSplitOptions.RemoveEmptyEntries);
                        // Calling a method with no parameters.
                        if (ThisCommand.Length == 1)
                        {
                            RunBracketedCommands(myBitmap, methods.GetMethod(ThisCommand[0]));
                        }
                        // Else calling a method with parameters.
                        else
                        {
                            UserVariables savedVariables = userVariables;

                            String[] methodCommands = methods.GetMethod(ThisCommand[0]);
                            userVariables = methods.GetMethodVariables(ThisCommand[0]);
                            List<String> initialMethodParams = methods.GetInitialMethodParams(ThisCommand[0]);
                            List<String> methodParamNames = userVariables.VarNameList;
                            List<String> extractedMethodParams = CommandChecker.Instance.ExtractMethodParameters(command);                            
                            if (initialMethodParams.Count != extractedMethodParams.Count)
                            {
                                throw new SyntaxException($"Error in {command}: Method requires {initialMethodParams.Count} variables, " +
                                    $"you have entered {extractedMethodParams.Count}");
                            }
                            userVariables.AddMethodParams(extractedMethodParams, savedVariables);

                            RunBracketedCommands(myBitmap, methodCommands);
                            
                            // After the method is done, restore the original.
                            savedVariables.SyncVariables(methodParamNames, userVariables);
                            userVariables = savedVariables;
                        }
                        break;
                    default:
                        // Something went wrong.
                        throw new SyntaxException($"Error: Error in RunCommand on the command: {command}.");
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine($"Index out of bounds");
                throw new SyntaxException($"Error: Index out of range in RunCommand on the command: {command}.");
            }
        }

        /// <summary>
        /// Command that runs all the commands within a box with the aid of a commandTypes array.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        /// <param name="commands">List of commands.</param>
        /// <param name="commandTypes">The array of command flags assigned to the list of commands</param>
        /// <remarks>
        /// ifCount is the index for if statements, increments by one everytime an if statement is run through RunCommands.
        /// loopCount is the index for loops, increments by one everytime an loop is fully run through RunCommands.
        /// isMethod is used to check if code is in a method bracket, loops and ifs inside a method cannot be accessed outside the method.
        /// </remarks>
        /// <exception cref="SyntaxException">
        /// Thrown when commandType is not equal to any of the cases, resulting in a syntax error.
        /// Also thrown if any of the commandType array elements equal error.
        /// </exception>
        public void RunCommands(Bitmap myBitmap, String[] commands, String[] commandTypes)
        {
            CommandTypes = commandTypes;
            
            // Index for if statements, increments by one everytime an if statement is run through RunCommands
            int ifCount = 0;
            int loopCount = 0;
            int index = 0;
            // If program is in a method it shouldn't save the loops and ifs outside of the method.
            bool inMethod = false;

            foreach (String commandType in commandTypes)
            {
                if (commandType.Equals("Error"))
                {
                    MessageBox.Show($"Syntax error in command: {commands[index]}");
                    Reset();
                    throw new SyntaxException($"Error: An error was flagged in the command types on line: {index}");
                }
                index++;
            }
            index = 0;
            foreach (String command in commands)
            {
                SplitCommand = command.Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);

                switch (commandTypes[index])
                {
                    case "Basic":
                        CheckCommand(command);
                        RunCommand(myBitmap, command);
                        break;
                    case "Single":
                        CheckCommand(command);
                        RunCommand(myBitmap, command);
                        break;
                    case "CreateVar":
                        CheckCommand(command);
                        RunCommand(myBitmap, command);
                        break;
                    case "CheckVar":
                        if (userVariables.CheckVariable(SplitCommand[0]))
                        {
                            CheckCommand(command);
                            RunCommand(myBitmap, command);
                        }
                        break;
                    case "CallMethod":
                        CheckCommand(command);
                        RunCommand(myBitmap, command);
                        break;
                    case "If":
                        if (inMethod)
                            break;
                        userVariables.CreateIfStatement(commands, index);
                        if (userVariables.CheckIfStatement(ifCount))
                        {
                            RunBracketedCommands(myBitmap, userVariables.GetIfStatement(ifCount));
                            ifCount++;
                        }
                        break;
                    case "Loop":
                        if (inMethod)
                            break;
                        userVariables.CreateLoop(commands, index);
                        for (int i = 0; i < userVariables.GetLoopNumber(loopCount); i++)
                            RunBracketedCommands(myBitmap, userVariables.GetLoop(loopCount));
                        loopCount++;
                        break;
                    case "Method":
                        SplitCommand = command.Split(seperator1, StringSplitOptions.RemoveEmptyEntries);
                        if (SplitCommand.Length == 2)
                        {
                            methods.CreateMethod(SplitCommand[1], commands, index);
                            inMethod = true;
                        }
                        else if (SplitCommand.Length > 3)
                        {
                            List<String> methodParameterNames = CommandChecker.Instance.ExtractMethodParameterNames(command);
                            methods.CreateMethod(SplitCommand[1], methodParameterNames, commands, index);
                            inMethod = true;
                        }                                             
                        break;
                    case "Endif":
                        // This is fine, nothing happens.
                        break;
                    case "Endloop":
                        // This is fine, nothing happens.
                        break;
                    case "Endmethod":
                        inMethod = false;
                        // This is fine, nothing happens.
                        break;
                    case "False":
                        // This is a command within a loop and shouldn't be run yet.
                        break;
                    default:
                        throw new SyntaxException($"Error: Error in RunCommands on the command: {command}.");
                }
                index++;
            }
        }

        /// <summary>
        /// Method for iterating through a single line command
        /// This also sets the CommandFlag, CommandParams and Parameters for that command.
        /// </summary>
        /// <param name="command">The command to be iterated through.</param>
        /// <remarks>
        /// ParseInteger is run from here to get the Parameters.
        /// </remarks>
        public void CheckCommand(String command)
        {
            // Command is split like so:
            // "[Rectangle] [50 60]"
            SplitCommand = command.Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                switch (CommandChecker.Instance.CheckCommand(SplitCommand[0]))
                {
                    case "Basic":
                        if (SplitCommand.Length == 2)
                        {
                            CommandType = "Basic";
                            CommandParams = SplitCommand[1].Split(seperator2, StringSplitOptions.RemoveEmptyEntries);
                            ParseInteger();
                        }
                        else CommandType = "Error";
                        break;
                    case "Single":
                        if (SplitCommand.Length == 1)
                        {
                            CommandType = "Single";
                        }
                        else CommandType = "Error";
                        break;
                    case "CreateVar":
                        /* 1st part is the name, 2nd part is the variable value (if any).
                        * eg "Var Radius = 10" becomes "[Var] [Radius = 10]" and run through the 1st if statement.
                        * Then becomes "[Radius] [= 10]" and run through the 2nd if statement.
                        * Then becomes "[10]" and is run through ParseInteger.
                        */
                        if (SplitCommand.Length == 2)
                        {
                            CommandType = "CreateVar";
                            SplitCommand = SplitCommand[1].Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);
                            if (SplitCommand.Length == 2)
                            {
                                CommandParams = SplitCommand[1].Split(seperator3, 2, StringSplitOptions.RemoveEmptyEntries);
                                ParseInteger();
                            }
                        }
                        else CommandType = "Error";
                        // Else variable will be created with initial value of 0.
                        break;
                    case "CallMethod":
                        if (CommandChecker.Instance.CheckSyntax(command, "callmethod"))
                            CommandType = "CallMethod";
                        else CommandType = "Error";
                        break;
                    case "CheckVar":
                        SplitCommand = command.Split(seperator1, 3, StringSplitOptions.RemoveEmptyEntries);
                        if (SplitCommand.Length == 3)
                        {
                            if (SplitCommand[1].Equals("+") || SplitCommand[1].Equals("=") || SplitCommand[2].Equals("-"))
                            {
                                if (userVariables.CheckVariable(SplitCommand[0]))
                                {
                                    SplitCommand = command.Split(seperator5, StringSplitOptions.RemoveEmptyEntries);
                                    CommandType = "EditVar";
                                    CommandParams = SplitCommand[1].Split(seperator5, 2, StringSplitOptions.RemoveEmptyEntries);
                                    ParseInteger();
                                    break;
                                }
                            }
                        }
                        else CommandType = "Error";
                        break;
                    default:
                        throw new SyntaxException($"Error: Error in RunCommands on the command: {command}.");
                }
            }
            catch (IndexOutOfRangeException)
            {
                throw new SyntaxException($"Error: Index out of bounds on command: {command}.");
            }
        }

        /// <summary>
        /// Method for parsing the strings in commandParams into integers.
        /// It uses the CommandParams array parses the outcome into Parameters.
        /// </summary>
        /// <remarks>
        /// It's private because it uses the internal variables commandParams and Parameters.
        /// If commandParams can't be parsed, it may be a variable, so it tries to get a variable instead.
        /// </remarks>
        /// <exception cref="SyntaxException">
        /// Thrown when one of the commandParams could not be parsed or assigned to a variable.
        /// </exception>
        private void ParseInteger()
        {
            for (int i = 0; i < CommandParams.Length && i < maximumParams; i++)
            {
                try
                {
                    Parameters[i] = Int32.Parse(CommandParams[i]);
                }
                catch (FormatException)
                {
                    // If it's a format exception, check if it is a variable.
                    if (userVariables.CheckVariable(CommandParams[i]))
                        Parameters[i] = userVariables.GetVariable(CommandParams[i]);
                    else throw new SyntaxException($"Variable {CommandParams[i]} does not exist.");
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine($"Index out of bounds");
                    throw new IndexOutOfRangeException();
                }
            }
        }

        

        /// <summary>
        /// Run commands from within the program
        /// </summary>
        /// <param name="myBitmap"></param>
        /// <param name="commands"></param>
        /// <remarks>
        /// Used when running commands from a loop, if statement or method.
        /// </remarks>
        private void RunBracketedCommands(Bitmap myBitmap, String[] commands)
        {
            String[] commandTypes = CommandChecker.Instance.IterateCommands(commands);
            RunCommands(myBitmap, commands, commandTypes);
        }

        /// <summary>
        /// Resets the if statements and loops within userVariables
        /// Also sets internal parameters to null;
        /// </summary>
        public void Reset()
        {
            userVariables.ResetIfsAndLoops();
            SplitCommand = null;
            CommandType = null;

            CommandTypes = null;
        }
    }
}
