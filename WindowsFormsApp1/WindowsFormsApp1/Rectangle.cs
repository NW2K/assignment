﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{
    /// <summary>
    /// Drawing class for drawing a rectangle.
    /// </summary>
    public class Rectangle : DrawingCommand
    {
        /// <summary>
        /// Width of the rectangle.
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Length of the rectangle.
        /// </summary>
        public int Length { get; private set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Rectangle()
        {
            SetPosition(0, 0);
            Width = 0;
            Length = 0;
        }

        /// <summary>
        /// Constructor where the x, y, width and length values are put in.
        /// </summary>
        /// <param name="x">The x value of the point where the rectangle will be drawn.</param>
        /// <param name="y">The y value of the point where the rectangle will be drawn.</param>
        /// <param name="width">The width of the rectangle.</param>
        /// <param name="length">The length of the rectangle.</param>
        public Rectangle(int x, int y, int width, int length)
        {
            SetPosition(x, y);
            this.Width = width;
            this.Length = length;
        }

        /// <summary>
        /// Method used to change the x, y, width and length values after construction.
        /// </summary>
        /// <param name="x">The x value of the point where the rectangle will be drawn.</param>
        /// <param name="y">The y value of the point where the rectangle will be drawn.</param>
        /// <param name="width">The width of the rectangle.</param>
        /// <param name="length">The length of the rectangle.</param>
        /// <param name="dummy">Dummy variable.</param>
        public override void SetValues(int x, int y, int width, int length, int dummy)
        {
            SetPosition(x, y);
            this.Width = width;
            this.Length = length;
        }

        /// <summary>
        /// Runs the program and draws a rectangle.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        public override void RunCommand(Bitmap myBitmap)
        {
            Pen myPen = new Pen(Color.Black, 5);

            Graphics g = Graphics.FromImage(myBitmap);
            g.DrawRectangle(myPen, X, Y, Width, Length);
            myPen.Dispose();
        }
    }
}
