﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{
    /// <summary>
    /// Drawing class for moving the pen.
    /// This class doesn't actually draw anything.
    /// </summary>
    class MoveTo : DrawingCommand
    {
        /// <summary>
        /// New x position.
        /// </summary>
        public int newX { get; private set; }

        /// <summary>
        /// New y position.
        /// </summary>
        public int newY { get; private set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MoveTo()
        {
            SetPosition(0, 0);
            newX = 0;
            newY = 0;
        }

        /// <summary>
        /// Constructor where the x and y values are put in
        /// </summary>
        /// <param name="x">The x value of point 1</param>
        /// <param name="y">The y value of point 1</param>
        /// <param name="addX">The value to be added onto x to make point 2.</param>
        /// <param name="addY">The value to be added onto y to make point 2.</param>
        public MoveTo(int x, int y, int addX, int addY)
        {
            SetPosition(x, y);
            this.newX = x + addX;
            this.newY = y + addY;
        }

        /// <summary>
        /// Method used to change the x, y, newX and newY values after construction.
        /// </summary>
        /// <param name="x">The x value of point 1</param>
        /// <param name="y">The y value of point 1</param>
        /// <param name="addX">The value to be added onto x to make point 2.</param>
        /// <param name="addY">The value to be added onto y to make point 2.</param>
        /// <param name="dummy">Dummy variable.</param>
        public override void SetValues(int x, int y, int addX, int addY, int dummy2)
        {
            SetPosition(x, y);
            this.newX = x + addX;
            this.newY = y + addY;
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        public override void RunCommand(Bitmap myBitmap)
        {
            // Does nothing.
        }
    }
}
