﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProgram
{
    // Is a singleton.
    public sealed class CommandChecker
    {
        private static CommandChecker instance = null;
        /// <summary>
        /// Singleton instance of CommandChecker.
        /// </summary>
        public static CommandChecker Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CommandChecker();
                }
                return instance;
            }
        }

        /// <summary>
        /// Types of basic commands that can take in parameters.
        /// </summary>
        private readonly string[] basicCommands = { "MoveTo", "DrawTo", "Rectangle", "Circle", "CircleFill", "Triangle", "Polygon" };
        /// <summary>
        /// Types of basic commands that don't take in parameters.
        /// </summary>
        private readonly string[] singleCommands = { "Clear", "Reset", "Save", "Load", "Help" };

        /// <summary>
        /// Seperates spaces in a command.
        /// Used to seperate the command type from the parameters
        /// </summary>
        private String[] seperator1 = { " " };
        /// <summary>
        /// Seperates spaces and left brackets in a command.
        /// Used in method creation and calling.
        /// </summary>
        private String[] seperator2 = { " ", "("};
        /// <summary>
        /// Seperates spaces and commas in a command.
        /// Used to seperate parameters.
        /// </summary>
        private String[] seperator3 = { " ", "," };
        /// <summary>
        /// Seperates spaces, commas and brackets in a command.
        /// Used to seperate the command when it is creating or calling a method.
        /// </summary>
        private String[] seperator4 = { " ", "(", ")", "," };

        /// <summary>
        /// Checks the command against various lists of commands types.
        /// If it's none of them, could be a variable.
        /// </summary>
        /// <param name="command">The command to be checked.</param>
        /// <returns>The type of command.</returns>
        public String CheckCommand(string command)
        {
            if (CheckBasicCommand(command))
                return "Basic";
            if (CheckSingleCommand(command))
                return "Single";
            if (command.Equals("Var", StringComparison.OrdinalIgnoreCase))
                return "CreateVar";
            if (command.Equals("Call", StringComparison.OrdinalIgnoreCase))
                return "CallMethod";
            else
                return "CheckVar";
        }

        /// <summary>
        /// Checks if the command is basic.
        /// </summary>
        /// <param name="command">The command to be checked.</param>
        /// <returns>True if the command is basic, false if not.</returns>
        public bool CheckBasicCommand(string command)
        {
            foreach (string basicCommand in basicCommands)
            {
                if (command.Equals(basicCommand, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if the command is a singular command that requires no parameters.
        /// </summary>
        /// <param name="command">The command to be checked.</param>
        /// <returns>True if the command is singular, false if not.</returns>
        public bool CheckSingleCommand(string command)
        {
            foreach (string singleCommand in singleCommands)
            {
                if (command.Equals(singleCommand, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks the command in detail to see what type it is.
        /// </summary>
        /// <param name="command">Command</param>
        /// <returns>Flag stating the command type</returns>
        public string CheckCommandDetailed(String command)
        {
            string[] ThisCommand = command.Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);

            switch (ThisCommand[0].ToLower())
            {
                case "if":                   
                    return "if";
                case "loop":
                    return "loop";
                case "method":                 
                    return "method";
                case "endif":
                    return "endif";
                case "endloop":
                    return "endloop";
                case "endmethod":
                    return "endmethod";
                case "var":
                    return "var";
                case "call":
                    return "callmethod";
                default:
                    if (CommandChecker.Instance.CheckCommand(ThisCommand[0]).Equals("Basic"))
                        return "basic";
                    if (CommandChecker.Instance.CheckCommand(ThisCommand[0]).Equals("Single"))
                        return "single";
                    return "checkvar";
            }
        }

        /// <summary>
        /// Runs through all the commands input, and determines whether they can be run as is, or are part of a statement.
        /// </summary>
        /// <param name="commands">Commands to be iterated though.</param>
        /// <returns>An array containing flags for each line of the command.</returns>
        /// <remarks>
        /// A stack: currentBracket is used to check whether all the brackets are closed, it also checks which bracket the code in currently in.
        /// commandFlag is true while outside of a bracket, but false while in a bracket.
        /// </remarks>
        /// <exception cref="SyntaxException">
        /// Thrown during a syntax error, there are multiple kinds of syntax errors that can be found in this code.
        /// </exception>
        public String[] IterateCommands(String[] commands)
        {
            String[] commandTypes = new String[commands.Length];

            bool commandFlag = true;
            int index = 0;

            bool inMethod = false;
            Stack<String> currentBracket = new Stack<String>();

            foreach (String command in commands)
            {
                String commandType = CheckCommandDetailed(command);
                switch (commandType)
                {
                    case "basic":
                        if (commandFlag == true)
                            commandTypes[index] = "Basic";
                        else
                            commandTypes[index] = "False";
                        break;
                    case "single":
                        if (commandFlag == true)
                            commandTypes[index] = "Single";
                        else
                            commandTypes[index] = "False";
                        break;
                    case "checkvar":
                        if (commandFlag == true)
                            commandTypes[index] = "CheckVar";
                        else
                            commandTypes[index] = "False";
                        break;
                    case "if":
                        if (CheckSyntax(command, commandType))
                        {
                            Console.WriteLine("If");
                            currentBracket.Push("If");
                            commandTypes[index] = "If";
                            commandFlag = false;
                        }
                        else throw new SyntaxException($"Error at line {index} ({command}): Syntax error.");
                        break;
                    case "loop":
                        if (CheckSyntax(command, commandType))
                        {
                            currentBracket.Push("Loop");
                            commandTypes[index] = "Loop";
                            commandFlag = false;
                        }
                        else throw new SyntaxException($"Error at line {index} ({command}): Syntax error.");
                        break;
                    case "method":
                        if (CheckSyntax(command, commandType))
                        {
                            if (inMethod)
                            {
                                // If something like this happens, the program can't be run, period.
                                commandTypes[index] = "Error";
                                throw new SyntaxException($"Error at {command}: Can't create a method while in a method.");
                            }
                            else if (currentBracket.Count > 0)
                            {
                                if (currentBracket.Peek().Equals("If") || currentBracket.Peek().Equals("Loop"))
                                {
                                    // Can't create a method while in a loop or if.
                                    commandTypes[index] = "Error";
                                    throw new SyntaxException($"Error at line {index} ({command}): Can't create a method while in an If or Loop.");
                                }
                            }
                            else
                            {
                                inMethod = true;
                                currentBracket.Push("Method");
                                commandTypes[index] = "Method";
                                commandFlag = false;
                            }
                        }
                        else throw new SyntaxException("Error: Syntax error in method declaration.");
                        break;
                    case "endif":
                        if (CheckSyntax(command, commandType))
                        {
                            if (currentBracket.Peek().Equals("If"))
                            {
                                currentBracket.Pop();
                                commandTypes[index] = "Endif";
                                commandFlag = true;
                            }
                            else
                            {
                                // Current bracket is not an if.
                                commandTypes[index] = "Error";
                                throw new SyntaxException($"Error at line {index} ({command}): Tried to endif while not in an if statement.");
                            }
                        }
                        else commandTypes[index] = "False";
                        break;
                    case "endloop":
                        if (CheckSyntax(command, commandType))
                        {
                            if (currentBracket.Peek().Equals("Loop"))
                            {
                                currentBracket.Pop();
                                commandTypes[index] = "Endloop";
                                commandFlag = true;
                            }
                            else
                            {
                                // Current bracket is not a loop
                                commandTypes[index] = "Error";
                                throw new SyntaxException($"Error at line {index} ({command}): Tried to endloop while not in an loop.");
                            }
                        }
                        else commandTypes[index] = "False";
                        break;
                    case "endmethod":
                        if (CheckSyntax(command, commandType))
                        {
                            if (inMethod && currentBracket.Peek().Equals("Method"))
                            {
                                currentBracket.Pop();
                                commandTypes[index] = "Endmethod";
                                commandFlag = true;
                            }
                            else throw new SyntaxException($"Error at line {index} ({command}): Tried to endmethod while not in an method.");
                        }
                        else throw new SyntaxException($"Error at line {index} ({command}): Syntax error.");
                        break;
                    case "var":
                        if (commandFlag == true)
                            commandTypes[index] = "CreateVar";
                        else commandTypes[index] = "False";
                        break;                  
                    case "callmethod":
                        if (CheckSyntax(command, commandType))
                        {
                            if (commandFlag == true)
                                commandTypes[index] = "CallMethod";
                            else commandTypes[index] = "False";
                        }
                        break;
                    default:
                        commandTypes[index] = "Error";
                        throw new SyntaxException($"Error at line {index} ({command}): Syntax error.");
                }
                index++;
            }
            if (currentBracket.Count > 0)
                throw new SyntaxException("Error: User forgot to close their brackets with end statements.");
            return commandTypes;
        }

        /// <summary>
        /// Method that takes in a command that creates a method with parameters, and extracts the parameter names.
        /// </summary>
        /// <param name="command">Command to be extracted (Must be a CreateMethod)</param>
        /// <returns>List of method parameter names</returns>
        public List<String> ExtractMethodParameterNames(String command)
        {
            List<String> parameterNames = new List<string>();
            // First split it to remove the "Method myMethod etc"
            String[] splitCommand = command.Split(seperator1, 3, StringSplitOptions.RemoveEmptyEntries);
            splitCommand = splitCommand[2].Split(seperator4, StringSplitOptions.RemoveEmptyEntries);
            bool varCheck = true;
            foreach (String param in splitCommand)
            {
                // Parameters in the paranthesis should alternate between var and the var name.
                if (varCheck)
                {
                    varCheck = false;
                }
                else
                {
                    parameterNames.Add(param);
                    varCheck = true;
                }
            }
            return parameterNames;
        }

        /// <summary>
        /// Method that takes in a command that calls a method with parameters, and extracts the parameter values.
        /// </summary>
        /// <param name="command">Command to be extracted (Must be a CallMethod)</param>
        /// <returns>List of method parameter values</returns>
        public List<String> ExtractMethodParameters(String command)
        {
            List<String> parameters = new List<string>();
            // First split it to remove the "Call myMethod etc"
            String[] splitCommand = command.Split(seperator4, 3, StringSplitOptions.RemoveEmptyEntries);
            splitCommand = splitCommand[2].Split(seperator4, StringSplitOptions.RemoveEmptyEntries);
            foreach (String param in splitCommand)
            {
                parameters.Add(param);
            }
            return parameters;
        }

        /// <summary>
        /// Iterates through a command to check if the syntax is correct.
        /// </summary>
        /// <param name="command">The command to be checked for syntax</param>
        /// <param name="commandType">The commandType syntax to be checked.</param>
        /// <returns>True if syntax is correct, false if incorrect.</returns>
        public bool CheckSyntax(String command, String commandType)
        {
            String[] splitCommand = command.Split(seperator1, StringSplitOptions.RemoveEmptyEntries);            
            switch (commandType)
            {
                case "if":
                    if (splitCommand.Length == 4)
                        if (splitCommand[0].Equals("If", StringComparison.OrdinalIgnoreCase))
                            if (splitCommand[2].Equals("<") || splitCommand[2].Equals("=") || splitCommand[2].Equals(">"))
                                return true;
                    return false;
                case "loop":
                    if (splitCommand.Length == 3)
                        if (splitCommand[0].Equals("Loop", StringComparison.OrdinalIgnoreCase))
                            if (splitCommand[1].Equals("for", StringComparison.OrdinalIgnoreCase))
                                return true;
                    return false;
                case "method":
                    if (splitCommand[0].Equals("Method", StringComparison.OrdinalIgnoreCase))
                    {
                        if (splitCommand.Length == 2)
                            return true;

                        // Split the command into two: Method and Method declaration.
                        splitCommand = command.Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);
                        // Further split the command into Method declaration and called parameters.
                        splitCommand = splitCommand[1].Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);
                        if (splitCommand.Length == 2)
                        {
                            //splitCommand = command.Split(seperator1, 4, StringSplitOptions.RemoveEmptyEntries);
                            // Checking if the paranthesis are in the right place.
                            if (splitCommand[1].StartsWith("(") && splitCommand[1].EndsWith(")"))
                            {
                                bool varCheck = true;
                                splitCommand = splitCommand[1].Split(seperator4, StringSplitOptions.RemoveEmptyEntries);
                                foreach (String param in splitCommand)
                                {
                                    // Parameters in the paranthesis should alternate between var and the var name.
                                    if (varCheck)
                                    {
                                        if (!param.Equals("var"))
                                            return false;
                                        varCheck = false;
                                    }
                                    else varCheck = true;
                                }
                                // varCheck should be true because the last object in the loop should be a variable name, not a var declaration.
                                if (varCheck)
                                    return true;
                            }
                        }
                    }
                    return false;
                case "endif":
                    if (splitCommand.Length == 1)
                        return true;
                    return false;
                case "endloop":
                    if (splitCommand.Length == 1)
                        return true;
                    return false;
                case "endmethod":
                    if (splitCommand.Length == 1)
                        return true;
                    return false;
                case "var":
                    if (splitCommand.Length == 2)
                        return true;
                    else if (splitCommand.Length == 4)
                        if (splitCommand[2].Equals("+") || splitCommand[2].Equals("=") || splitCommand[2].Equals("-"))
                            return true;
                    return false;
                case "callmethod":
                    if (splitCommand[0].Equals("Call", StringComparison.OrdinalIgnoreCase))
                    {
                        if (splitCommand.Length == 2)
                             return true;

                        // Split the command into two: Call and Method declaration.
                        splitCommand = command.Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);
                        // Further split the command into Method declaration and called parameters.
                        splitCommand = splitCommand[1].Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries);
                        if (splitCommand.Length > 1)
                        {
                            if (splitCommand[1].StartsWith("(") && splitCommand[1].EndsWith(")"))
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                default:
                    return false;
            }
        }
    }
}
