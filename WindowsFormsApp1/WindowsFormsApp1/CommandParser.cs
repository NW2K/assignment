﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProgram
{
    /// <summary>
    /// Class used to parse the command from the form, forwards it to the CommandSplitter class and so on.
    /// This is the class that catches any SyntaxExceptions thrown from lower classes and throws up an appropriate MessageBox.
    /// </summary>
    public class CommandParser
    {
        /// <summary>
        /// The main CommandSplitter class.
        /// </summary>
        private CommandSplitter commandSplitter = new CommandSplitter();

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public CommandParser()
        {

        }

        /// <summary>
        /// Checks the text input into the CommandLine and runs it through CommandSplitter class to split it.
        /// If the "Run" command is input it goes to RunCommands.
        /// </summary>
        /// <param name="myBitmap">A single line of code passed through, can be through commandLine or commandBox</param>
        /// <param name="command">Current line of code being run.</param>
        /// <param name="commandBoxText">Text within the commandBox.</param>
        /// <remarks>
        /// Catches any SyntaxExceptions in commandSplitter.
        /// commandBoxText is used in the constructor because this method can potentially call RunCommands.
        /// If the command equals run we run the code in the commandBoxText. 
        /// </remarks>
        public void RunCommand(Bitmap myBitmap, String command, String commandBoxText)
        {
            if (command.Equals("Run", StringComparison.OrdinalIgnoreCase))
            {
                RunCommands(myBitmap, command, commandBoxText);
            }            
            else
            {
                try
                {
                    commandSplitter.CheckCommand(command);
                    commandSplitter.RunCommand(myBitmap, command);
                }
                catch (SyntaxException e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        /// <summary>
        /// Splits the text in RunCommands into individual commands to be run through RunCommand.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        /// <param name="commandLineText">Text within the commandLine</param>
        /// <param name="commandBoxText">Text within the commandBox</param>
        /// <remarks>
        /// Catches any SyntaxExceptions in commandSplitter.
        /// commandLineText is used in the constructor because this method can potentially call RunCommand.
        /// </remarks>
        public void RunCommands(Bitmap myBitmap, String commandLineText, String commandBoxText)
        {
            // Instead of running commands, store them first.
            String[] commands = commandBoxText.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.RemoveEmptyEntries);
            
            try
            {
                // Check the array for statements & syntax, first loop.
                String[] commandTypes = CommandChecker.Instance.IterateCommands(commands);
                // Then run it. Second loop.
                commandSplitter.RunCommands(myBitmap, commands, commandTypes);
            }
            catch (SyntaxException e)
            {
                MessageBox.Show(e.Message);
            }
            // After this, we are done with the created ifs and loops and don't need them anymore and thus can reset them.
            commandSplitter.Reset();
        }
    }
}
