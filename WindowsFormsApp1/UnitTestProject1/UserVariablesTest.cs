﻿using System;
using DrawingProgram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DrawingProgramTest
{
    [TestClass]
    public class UserVariablesTest
    {
        /// <summary>
        /// Checks that if a variable is normally created, no errors are thrown.
        /// </summary>
        [TestMethod]
        public void CreateVariable_CreatesVariable_NoError()
        {
            UserVariables userVariables = new UserVariables();
            userVariables.CreateVariable("Radius", 32);
        }

        /// <summary>
        /// Check that that after creating a variable, using GetVariable should return that variable using the same name.
        /// </summary>
        [TestMethod]
        public void CreateVariable_ThenGetVariable_ReturnsVariable()
        {
            UserVariables userVariables = new UserVariables();
            userVariables.CreateVariable("Radius", 32);
            int variable = userVariables.GetVariable("Radius");
            Assert.AreEqual(32, variable);
        }

        /// <summary>
        /// Checks that creating a variable with a negative integer works fine.
        /// </summary>
        [TestMethod]
        public void CreateVariable_CreateNegativeIntAndGetVariable_ReturnsVariable()
        {
            UserVariables userVariables = new UserVariables();
            userVariables.CreateVariable("Radius", -32);
            int variable = userVariables.GetVariable("Radius");
            Assert.AreEqual(-32, variable);
        }

        /// <summary>
        /// Checks that using an invalid name, regardless of case, while trying to create a variable will throw an exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void CreateVariable_UseInvalidName_ThrowsException()
        {
            UserVariables userVariables = new UserVariables();
            try
            {                
                userVariables.CreateVariable("Circle", 32);
            }
            catch (Exception)
            {
                try
                {
                    userVariables.CreateVariable("CiRcLe", 32);
                }
                catch (Exception) { Assert.Fail(); }
            }
        }

        /// <summary>
        /// Checks that creating two variables with the same name will throw an exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void CreateVariable_CreateVariableTwice_ThrowsException()
        {
            UserVariables userVariables = new UserVariables();
            userVariables.CreateVariable("Radius", 32);
            try
            {
                userVariables.CreateVariable("Radius", 64);
            }
            catch (Exception) { Assert.Fail(); }
        }

        /// <summary>
        /// Checks that creating two variables with the same name but different case will create two different variables.
        /// </summary>
        [TestMethod]
        public void CreateVariable_CreateVariableTwiceInDifferentCaseThenGetVariable_ReturnsTwoDifferentVariables()
        {
            UserVariables userVariables = new UserVariables();
            userVariables.CreateVariable("Radius", 32);
            userVariables.CreateVariable("radius", 64);
            int variable1 = userVariables.GetVariable("Radius");
            int variable2 = userVariables.GetVariable("radius");
            Assert.AreEqual(32, variable1);
            Assert.AreEqual(64, variable2);
        }

        /// <summary>
        /// Checks that creating a variable, then trying to retrieve it with a name in different case will throw an exception.
        /// Because variable names are case sensitive
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void CreateVariable_ThenGetVariableWithDifferentCaseName_ThrowsException()
        {
            UserVariables userVariables = new UserVariables();
            userVariables.CreateVariable("Radius", 32);
            try
            {
                int variable1 = userVariables.GetVariable("radius");
            }
            catch (Exception) { Assert.Fail(); }
        }

        /// <summary>
        /// Checks that editting a variable should reflect properly in the class.
        /// </summary>
        [TestMethod]
        public void CreateVariable_ThenEditVariableThenGetVariable_ReturnsVariable()
        {
            int variable;
            UserVariables userVariables = new UserVariables();
            userVariables.CreateVariable("Radius", 32);
            userVariables.EditVariable("Radius", "+", 12);
            variable = userVariables.GetVariable("Radius");
            Assert.AreEqual(44, variable);

            userVariables.EditVariable("Radius", "-", 20);
            variable = userVariables.GetVariable("Radius");
            Assert.AreEqual(24, variable);

            userVariables.EditVariable("Radius", "=", 60);
            variable = userVariables.GetVariable("Radius");
            Assert.AreEqual(60, variable);
        }

        /// <summary>
        /// Checks that trying to edit a variable which does not exist throws an exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void EditVariable_VariableDoesNotExist_ThrowsException()
        {
            UserVariables userVariables = new UserVariables();
            try
            {
                userVariables.EditVariable("Radius", "+", 12);
            }
            catch (Exception) { Assert.Fail(); }
        }

        /// <summary>
        /// Checks that creating a variable, then checking if it exists should return as true.
        /// </summary>
        [TestMethod]
        public void CreateVariable_ThenCheckVariable_ReturnsTrue()
        {
            UserVariables userVariables = new UserVariables();
            userVariables.CreateVariable("Radius", 32);
            Assert.AreEqual(true, userVariables.CheckVariable("Radius"));
        }

        /// <summary>
        /// Checks that checking the existence of a variable which does not exist should return false.
        /// </summary>
        [TestMethod]
        public void CheckVariable_WhileVariableDoesNotExist_ReturnsFalse()
        {
            UserVariables userVariables = new UserVariables();
            Assert.AreEqual(false, userVariables.CheckVariable("Radius"));
        }

        /// <summary>
        /// Checks that creating an if statement with commands should result in no error.
        /// </summary>
        [TestMethod]
        public void CreateIfStatement_WithCommands_NoError()
        {
            UserVariables userVariables = new UserVariables();
            String[] commands = { "If 10 = 10", "Circle 40", "Rectangle 50 60", "Triangle 80 20", "Endif" };
            userVariables.CreateIfStatement(commands, 0);
        }

        /// <summary>
        /// Checks that creating an if statement with no endif should throw a syntax error.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void CreateIfStatement_WithNoEndif_ThrowsException()
        {
            UserVariables userVariables = new UserVariables();
            String[] commands = { "If 10 = 10", "Circle 40", "Rectangle 50 60", "Triangle 80 20" };
            try
            {
                userVariables.CreateIfStatement(commands, 0);
            }
            catch (Exception) { Assert.Fail(); }
        }

        /// <summary>
        /// Checks that creating an if statement, then trying to get it while the condition is valid
        /// should return the if statement commands.
        /// </summary>
        [TestMethod]
        public void CreateIfStatement_ThenGetIfStatementWithValidCondition_ReturnsIfStatement()
        {
            UserVariables userVariables = new UserVariables();
            String[] commands = { "If 10 = 10", "Circle 40", "Rectangle 50 60", "Triangle 80 20", "Endif" };
            userVariables.CreateIfStatement(commands, 0);
            String[] ifCommands = userVariables.GetIfStatement(0);
            Assert.AreEqual("Circle 40", ifCommands[0]);
            Assert.AreEqual("Rectangle 50 60", ifCommands[1]);
            Assert.AreEqual("Triangle 80 20", ifCommands[2]);
        }

        /// <summary>
        /// Checks that creating an if statement, then trying to get it while the condition is invalid
        /// should return null.
        /// </summary>
        [TestMethod]
        public void CreateIfStatement_ThenGetIfStatementWithInvalidCondition_ReturnsNull()
        {
            UserVariables userVariables = new UserVariables();
            String[] commands = { "If 10 = 5", "Circle 40", "Rectangle 50 60", "Triangle 80 20", "Endif" };
            userVariables.CreateIfStatement(commands, 0);
            String[] ifCommands = userVariables.GetIfStatement(0);
            Assert.AreEqual(null, ifCommands);
        }

        /// <summary>
        /// Checks that creating an loop with commands should result in no error.
        /// </summary>
        [TestMethod]
        public void CreateLoop_WithCommands_NoError()
        {
            UserVariables userVariables = new UserVariables();
            String[] commands = { "Loop for 8", "Circle 40", "Rectangle 50 60", "Triangle 80 20", "Endloop" };
            userVariables.CreateLoop(commands, 0);
        }

        /// <summary>
        /// Checks that creating an loop statement with no endloop should throw a syntax error.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void CreateLoop_WithNoEndloop_ThrowsException()
        {
            UserVariables userVariables = new UserVariables();
            String[] commands = { "Loop for 8", "Circle 40", "Rectangle 50 60", "Triangle 80 20"};
            try
            {
                userVariables.CreateLoop(commands, 0);
            }
            catch (Exception) { Assert.Fail(); }
        }

        /// <summary>
        /// Checks that creating a loop then getting it should return the loop commands.
        /// </summary>
        [TestMethod]
        public void CreateLoop_ThenGetLoop_ReturnsLoop()
        {
            UserVariables userVariables = new UserVariables();
            String[] commands = { "Loop for 8", "Circle 40", "Rectangle 50 60", "Triangle 80 20", "Endloop" };
            userVariables.CreateLoop(commands, 0);
            String[] loopCommands = userVariables.GetLoop(0);
            Assert.AreEqual("Circle 40", loopCommands[0]);
            Assert.AreEqual("Rectangle 50 60", loopCommands[1]);
            Assert.AreEqual("Triangle 80 20", loopCommands[2]);
        }

        /// <summary>
        /// Checks that creating a loop then getting the loop number should return the loop number.
        /// </summary>
        [TestMethod]
        public void CreateLoop_ThenGetLoopNumber_ReturnsLoopNumber()
        {
            UserVariables userVariables = new UserVariables();
            String[] commands = { "Loop for 8", "Circle 40", "Rectangle 50 60", "Triangle 80 20", "Endloop" };
            userVariables.CreateLoop(commands, 0);
            int loopNo = userVariables.GetLoopNumber(0);
            Assert.AreEqual(8, loopNo);
        }

        
    }
}
