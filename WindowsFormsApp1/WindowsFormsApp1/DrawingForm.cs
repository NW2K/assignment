﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProgram
{
    /// <summary>
    /// Main form class which gives the UI for accessing the program.
    /// </summary>
    public partial class DrawingForm : Form
    {
        /// <summary>
        /// Bitmap for program to paint on
        /// </summary>
        private Bitmap myBitmap;

        /// <summary>
        /// Individual command on a line.
        /// </summary>
        private String command;

        /// <summary>
        /// Main CommandParser class.
        /// </summary>
        private CommandParser commandParser = new CommandParser();

        /// <summary>
        /// Initial Constructor.
        /// Initialises the form.
        /// </summary>
        public DrawingForm()
        {
            InitializeComponent();
            commandButton.Text = "Run Command";
            myBitmap = new Bitmap(pictureBox.Size.Width, pictureBox.Size.Height);
        }

        /// <summary>
        /// Called from the invalidate method, paints the bitmap onto the PictureBox.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Provides data for this Control.Paint event.</param>
        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImageUnscaled(myBitmap, 0, 0);
        }

        /// <summary>
        /// Checks what the text in the CommandButton (and by extension DropDownList) is and
        /// runs the appropriate command.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event data.</param>
        private void commandButton_Click(object sender, EventArgs e)
        {
            if (commandButton.Text == ("Run Command"))
            {
                command = commandLine.Text;
                commandParser.RunCommand(myBitmap, command, commandBox.Text);
                pictureBox.Invalidate();
            }
            if (commandButton.Text == ("Save Program"))
            {         
                String[] commands = commandBox.Text.Split(new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.RemoveEmptyEntries);
                SaveFile saveFile = new SaveFile();
                saveFile.Save(commands);
            }
            if (commandButton.Text == ("Save Picture"))
            {
                SaveFile saveFile = new SaveFile();
                saveFile.Save(myBitmap);                
            }
            if (commandButton.Text == ("Load Program"))
            {
                LoadFile loadFile = new LoadFile();
                commandBox.Text = loadFile.Load(commandBox.Text);
            }
            if (commandButton.Text == ("Help"))
            {
                MessageBox.Show("Commands: \r" +
                    "moveTo <x>,<y> \r" +
                    "drawTo <x>,<y> \r" +
                    "Clear \r" +
                    "Reset \r" +
                    "Rectangle <width>,<height> \r" +
                    "Circle <radius> \r" +
                    "Triangle <width>,<height> \r \r" +
                    "-Advanced Commands- \r" +
                    "Var [Variable] {OR} Var [Variable] = [Integer] \r" +
                    "[Variable] [+ or = or -] [Integer] \r \r" +
                    "Loop for [Integer] \r" +
                    "[Commands] \r" +
                    "Endloop \r \r" +
                    "If [Integer] [> or = or <] [Integer] \r" +
                    "[Commands] \r" +
                    "Endif \r \r" +
                    "Method [myMethod] {OR} Method [myMethod] (var [Parameter], [etc...]) \r" +
                    "[Commands] \r" +
                    "Endmethod \r \r" +
                    "Call [myMethod] {OR} Call [myMethod] ([Integer], [etc...])", "Help");
            }
        }

        /// <summary>
        /// Runs when CommandLine detects an Enter input, the text in the CommandLine is run through
        /// RunCommand.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event data.</param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                command = commandLine.Text;
                commandParser.RunCommand(myBitmap, command, commandBox.Text);
                pictureBox.Invalidate();
            }
        }

        /// <summary>
        /// Runs when the CommandsButton is clicked, and goes to RunCommands method.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event Data.</param>
        private void commandsButton_Click(object sender, EventArgs e)
        {
            commandParser.RunCommands(myBitmap, commandLine.Text, commandBox.Text);
            pictureBox.Invalidate();
        }

        /// <summary>
        /// Occurs when the DropDownMenu is changed and changes the CommandButton text to be equal to it.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event Data</param>
        private void dropDownMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            commandButton.Text = dropDownMenu.Text;
        }
    }
}
