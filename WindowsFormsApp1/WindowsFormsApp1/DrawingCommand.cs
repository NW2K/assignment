﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{
    /// <summary>
    /// Superclass for shape drawing.
    /// </summary>
    public abstract class DrawingCommand
    {

        /// <summary>
        /// X Position
        /// </summary>
        public int X { get; private set; }

        /// <summary>
        /// Y Position
        /// </summary>
        public int Y { get; private set; }

        /// <summary>
        /// Sets the new x and y values
        /// </summary>
        /// <param name="x">New x position</param>
        /// <param name="y">New y position</param>
        public void SetPosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Set the appropriate values for the DrawingCommand.
        /// </summary>
        /// <param name="x">x position.</param>
        /// <param name="y">y position.</param>
        /// <param name="param1">Parameter 1.</param>
        /// <param name="param2">Parameter 2.</param>
        /// <param name="param3">Parameter 3.</param>
        public abstract void SetValues(int x, int y, int param1, int param2, int param3);

        /// <summary>
        /// Runs the drawing command.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        public abstract void RunCommand(Bitmap myBitmap);

    }
}
