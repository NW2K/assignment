﻿using System.Drawing;
using System.Windows.Forms;

namespace DrawingProgram
{
    partial class DrawingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.commandLine = new System.Windows.Forms.TextBox();
            this.commandBox = new System.Windows.Forms.RichTextBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.commandButton = new System.Windows.Forms.Button();
            this.commandsButton = new System.Windows.Forms.Button();
            this.dropDownMenu = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // commandLine
            // 
            this.commandLine.Location = new System.Drawing.Point(12, 6);
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(638, 20);
            this.commandLine.TabIndex = 0;
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLine_KeyDown);
            // 
            // commandBox
            // 
            this.commandBox.Location = new System.Drawing.Point(12, 33);
            this.commandBox.Name = "commandBox";
            this.commandBox.Size = new System.Drawing.Size(452, 266);
            this.commandBox.TabIndex = 1;
            this.commandBox.Text = "";
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Location = new System.Drawing.Point(470, 33);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(446, 295);
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            // 
            // commandButton
            // 
            this.commandButton.Location = new System.Drawing.Point(666, 5);
            this.commandButton.Name = "commandButton";
            this.commandButton.Size = new System.Drawing.Size(97, 21);
            this.commandButton.TabIndex = 3;
            this.commandButton.UseVisualStyleBackColor = true;
            this.commandButton.Click += new System.EventHandler(this.commandButton_Click);
            // 
            // commandsButton
            // 
            this.commandsButton.Location = new System.Drawing.Point(13, 304);
            this.commandsButton.Name = "commandsButton";
            this.commandsButton.Size = new System.Drawing.Size(451, 23);
            this.commandsButton.TabIndex = 4;
            this.commandsButton.Text = "Run Commands";
            this.commandsButton.UseVisualStyleBackColor = true;
            this.commandsButton.Click += new System.EventHandler(this.commandsButton_Click);
            // 
            // dropDownMenu
            // 
            this.dropDownMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownMenu.FormattingEnabled = true;
            this.dropDownMenu.Items.AddRange(new object[] {
            "Run Command",
            "Save Picture",
            "Save Program",
            "Load Program",
            "Help"});
            this.dropDownMenu.Location = new System.Drawing.Point(760, 5);
            this.dropDownMenu.Name = "dropDownMenu";
            this.dropDownMenu.Size = new System.Drawing.Size(156, 21);
            this.dropDownMenu.TabIndex = 5;
            this.dropDownMenu.SelectedIndexChanged += new System.EventHandler(this.dropDownMenu_SelectedIndexChanged);
            // 
            // DrawingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 340);
            this.Controls.Add(this.dropDownMenu);
            this.Controls.Add(this.commandsButton);
            this.Controls.Add(this.commandButton);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.commandBox);
            this.Controls.Add(this.commandLine);
            this.Name = "DrawingForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox commandLine;
        private System.Windows.Forms.RichTextBox commandBox;
        private System.Windows.Forms.PictureBox pictureBox;
        private Button commandButton;
        private Button commandsButton;
        private ComboBox dropDownMenu;
    }
}

