Commands are not case sensitive

moveTo x y
drawTo x y
Clear
Reset
Rectangle width height
Circle radius
Triangle width height

Commands on the single line can be run by pressing the button
on the right or by pressing enter
Commands in the box can be run by entering run into the single line or pressing the button on the bottom

-

Part 1 Functions Added
Saving and Loading (2 Marks)
Added more documentation on Unit Tests (Up to 2 more marks)
Plan on adding tests for Part 2 (Up to 3 more marks)


	-Advanced Commands-
Var [Variable] {OR} Var [Variable] = [Integer] \r
[Variable] [+ or = or -] [Integer] \r \r

Loop for [Integer]
[Commands]
Endloop

If [Integer] [> or = or <] [Integer]
[Commands]
Endif

Method [myMethod] {OR} Method [myMethod] (var [Parameter], [etc...])
[Commands]
Endmethod

Call [myMethod] {OR} Call [myMethod] ([Integer], [etc...]) 