﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{
    /// <summary>
    /// Drawing command that draws a triangle.
    /// </summary>
    public class Triangle : DrawingCommand
    {
        /// <summary>
        /// Width of the triangle.
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Height of the triangle.
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Triangle()
        {
            SetPosition(0, 0);
            Width = 0;
            Height = 0;
        }

        /// <summary>
        /// Constructor where the x, y, width and height values are put in.
        /// </summary>
        /// <param name="x">The x value of the point where the triangle will be drawn.</param>
        /// <param name="y">The y value of the point where the triangle will be drawn.</param>
        /// <param name="width">The width of the triangle.</param>
        /// <param name="height">The height of the triangle.</param>
        public Triangle(int x, int y, int width, int height)
        {
            SetPosition(x, y);
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Method used to change the x, y, width and height values after construction.
        /// </summary>
        /// <param name="x">The x value of the point where the triangle will be drawn.</param>
        /// <param name="y">The y value of the point where the triangle will be drawn.</param>
        /// <param name="width">The width of the triangle.</param>
        /// <param name="height">The height of the triangle.</param>
        /// <param name="dummy">Dummy variable.</param>
        public override void SetValues(int x, int y, int width, int length, int dummy)
        {
            SetPosition(x, y);
            Width = width;
            Height = length;
        }

        /// <summary>
        /// Runs the program and draws a triangle.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        public override void RunCommand(Bitmap myBitmap)
        {
            Point pt1 = new Point(X, Y);
            Point pt2 = new Point(X, Y + Height);
            Point pt3 = new Point(X + Width, Y + Height);
            Pen myPen = new Pen(Color.Black, 5);

            Graphics g = Graphics.FromImage(myBitmap);
            g.DrawLine(myPen, pt1, pt2);
            g.DrawLine(myPen, pt2, pt3);
            g.DrawLine(myPen, pt3, pt1);
            myPen.Dispose();
        }
    }
}
