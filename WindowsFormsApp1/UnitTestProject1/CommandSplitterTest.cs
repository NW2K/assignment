﻿using System;
using DrawingProgram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DrawingProgramTest
{
    /// <summary>
    /// Test class for CommandSplitter.
    /// </summary>
    [TestClass]
    public class CommandSplitterTest
    {
        /// <summary>
        /// Checks to see that splitting the string gives the appropriate substrings.
        /// </summary>
        [TestMethod]
        public void SplitCommand_SplitsRegularString_GivesExpectedValue()
        {
            CommandSplitter splitter = new CommandSplitter();
            String testString = "Rectangle 50 60";
            splitter.CheckCommand(testString);
            Assert.AreEqual("Rectangle", splitter.SplitCommand[0]);
            Assert.AreEqual("50", splitter.CommandParams[0]);
            Assert.AreEqual("60", splitter.CommandParams[1]);

        }

        /// <summary>
        /// Checks to see that even if a string has an abundance of spaces it is still split properly.
        /// </summary>
        [TestMethod]
        public void SplitCommand_SplitsStringWithTooManySpaces_GivesExpectedValue()
        {
            CommandSplitter splitter = new CommandSplitter();
            String testString = "  Rectangle  50  60  ";
            splitter.CheckCommand(testString);
            Assert.AreEqual("Rectangle", splitter.SplitCommand[0]);
            Assert.AreEqual("50", splitter.CommandParams[0]);
            Assert.AreEqual("60", splitter.CommandParams[1]);
        }

        /// <summary>
        /// Checks that if there is no spaces in the string, only one string is returned.
        /// </summary>
        [TestMethod]
        public void SplitCommand_SplitsStringWithNoSpaces_GivesExpectedValue()
        {
            CommandSplitter splitter = new CommandSplitter();
            String testString = "Rectangle5060";
            splitter.CheckCommand(testString);
            Assert.AreEqual("Rectangle5060", splitter.SplitCommand[0]);
        }

        /// <summary>
        /// Checks that if there are too many substrings in the string it ignores the excess strings.
        /// </summary>
        [TestMethod]
        public void SplitCommand_SplitsStringWithTooManySubstrings_IgnoresExcessStrings()
        {
            CommandSplitter splitter = new CommandSplitter();
            String testString = "Rectangle 50 60 70 80";
            splitter.CheckCommand(testString);
            Assert.AreEqual("Rectangle", splitter.SplitCommand[0]);
            Assert.AreEqual("50", splitter.CommandParams[0]);
            Assert.AreEqual("60", splitter.CommandParams[1]);
            Assert.AreEqual("70", splitter.CommandParams[2]);
        }

        /// <summary>
        /// Checks that the IndexOutOfRange exception is thrown when a part of the array outside of the
        /// maximum is accessed.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void SplitCommand_AccessOutOfBoundsInArray_ThrowsException()
        {
            CommandSplitter splitter = new CommandSplitter();
            String testString = "Rectangle 50 60";
            splitter.CheckCommand(testString);
            testString = splitter.CommandParams[99];
        }

        /// <summary>
        /// Checks that the string to int conversion is working properly when the proper value is given.
        /// </summary>
        [TestMethod]
        public void SplitCommand_TriesParsingIntToInt_GivesExpectedValue()
        {
            CommandSplitter splitter = new CommandSplitter();
            String testString = "Rectangle 50 60";
            splitter.CheckCommand(testString);
            Assert.AreEqual(50, splitter.Parameters[0]);
            Assert.AreEqual(60, splitter.Parameters[1]);
        }

        /// <summary>
        /// Checks that a Format Exception is thrown when the program tries to convert a string to an int.
        /// The String in this has also not been initialised as a variable.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void SplitCommand_TriesParsingStringToInt_ThrowsException()
        {
            try
            {
                CommandSplitter splitter = new CommandSplitter();
                String testString = "Rectangle fifty sixty";
                splitter.CheckCommand(testString);
            }
            catch (Exception) { Assert.Fail(); }
        }
    }
}
