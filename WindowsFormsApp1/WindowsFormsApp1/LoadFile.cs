﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProgram
{
    /// <summary>
    /// Class that is used to load a file.
    /// </summary>
    public class LoadFile
    {
        /// <summary>
        /// Main dialog for opening a file.
        /// </summary>
        private OpenFileDialog openFileDialog = new OpenFileDialog();

        /// <summary>
        /// Method used to load a program as a text file.
        /// </summary>
        /// <param name="commandBoxText">The text in the commandBox.</param>
        /// <returns>The program, returns the commandBoxText if nothing is found.</returns>
        public string Load(string commandBoxText)
        {
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.Title = "Load an Text File";
            openFileDialog.ShowDialog();
            if (openFileDialog.FileName != "")
            {
                try
                {
                    MessageBox.Show("Loaded program: " + openFileDialog.FileName);
                    return System.IO.File.ReadAllText(openFileDialog.FileName);
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("Error", "Cannot find file.");
                    return commandBoxText;
                }
                catch (IOException)
                {
                    MessageBox.Show("Error", "IO exception");
                    return commandBoxText;
                }

            }
            else
            {
                MessageBox.Show("Failed to load program" + openFileDialog.FileName);
                return commandBoxText;
            }
        }
    }
}
