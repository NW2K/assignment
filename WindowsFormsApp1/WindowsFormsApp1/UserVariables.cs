﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{

     /// <summary>
     /// Class that contains the user's variables, as well as stores the commands within ifs and loops.
     /// </summary>
    public class UserVariables
    {
        /// <summary>
        /// List of variable values.
        /// </summary>
        public List<int> VarIntList { get; private set; } = new List<int>();
        /// <summary>
        /// List of variable names.
        /// </summary>
        public List<String> VarNameList { get; private set; } = new List<String>();

        /// <summary>
        /// List of if statements.
        /// </summary>
        public List<String> IfList { get; private set; } = new List<String>();
        /// <summary>
        /// List of if statement conditions.
        /// </summary>
        public List<String> IfConditionList { get; private set; } = new List<String>();

        /// <summary>
        /// List of loops.
        /// </summary>
        public List<String> LoopList { get; private set; } = new List<String>();
        /// <summary>
        /// List of loop counts.
        /// </summary>
        public List<String> LoopCountList { get; private set; } = new List<String>();

        /// <summary>
        /// Index of the if statement array.
        /// </summary>
        private int ifIndex = 0;
        /// <summary>
        /// Index of the loop array.
        /// </summary>
        private int loopIndex = 0;

        /// <summary>
        /// Seperates with spaces.
        /// </summary>
        private readonly String[] seperator1 = { " " };

        /// <summary>
        /// String of invalid names for variables, these include all types of commands to mitigate confusion.
        /// </summary>
        private readonly String[] invalidNames = { "MoveTo", "DrawTo", "Rectangle", "Circle", "CircleFill",
            "Triangle", "Polygon", "Clear", "Reset", "Save", "Load", "Help", "If", "Loop",
            "Method", "Var", "Call", "Run" };
        // Used to sync up the names between variables and methods so that there is no overlap.

        /// <summary>
        /// Default constructor.
        /// </summary>
        public UserVariables()
        {

        }

        /// <summary>
        /// Function creates a key value pair between the name and variable (integer).
        /// </summary>
        /// <param name="name">Name of the variable.</param>
        /// <param name="variable">Value of the variable, an integer.</param>
        /// <exception cref="SyntaxException">
        /// Thrown if variable name is invalid.
        /// </exception>
        public void CreateVariable(String name, int variable)
        {
            if (CheckForInvalidName(name))
                throw new SyntaxException($"Error in variable ({name}): Name already exists.");

            bool exists = false;

            // Used if nothing has been added to list yet.
            if (VarNameList.Count == 0)
            {
                VarNameList.Add(name);
                VarIntList.Add(variable);
            }
            int index = 0;
            foreach (String key in VarNameList)
            {
                // If variable name already exists, replace it.
                if (key.Equals(name))
                {
                    VarIntList[index] = variable;
                    exists = true;
                }
                index++;
            }
            // If it does not exist, add to it.
            if (exists == false)
            {
                VarNameList.Add(name);
                VarIntList.Add(variable);
            }
        }

        /// <summary>
        /// Edits a known variable in the array.
        /// </summary>
        /// <param name="name">Name of the variable.</param>
        /// <param name="varOperator">Operator applied to the variable, is either +, - or =.</param>
        /// <param name="parameter">Parameter that will modify the variable.</param>
        /// <exception cref="SyntaxException">
        /// Thrown if variable name doesn't exist.
        /// </exception>
        public void EditVariable(String name, String varOperator, int parameter)
        {
            int index = 0;
            foreach (String key in VarNameList)
            {                   
                // If variable name already exists, replace it.
                if (key.Equals(name))
                {
                    if (varOperator.Equals("+"))
                        VarIntList[index] += parameter;
                    if (varOperator.Equals("="))
                        VarIntList[index] = parameter;
                    if (varOperator.Equals("-"))
                        VarIntList[index] -= parameter;
                    return;
                }
                index++;
            }
            throw new SyntaxException($"Error in UserVariables: Variable {name} doesn't exist.");
        }

        /// <summary>
        /// Returns a known variable in the array.
        /// </summary>
        /// <param name="name">Name of the variable.</param>
        /// <returns>The variable value.</returns>
        /// <exception cref="SyntaxException">
        /// Thrown if variable name doesn't exist.
        /// </exception>
        public int GetVariable(String name)
        {
            int index = 0;
            int variable = 0;
            foreach (String key in VarNameList)
            {
                if (key.Equals(name) && VarNameList.Count != 0)
                {
                    variable = VarIntList[index];
                    return variable;
                }
                index++;
            }
            throw new SyntaxException($"Error in UserVariables: Variable {name} doesn't exist.");
        }

        /// <summary>
        /// Checks if a particular variable exists.
        /// </summary>
        /// <param name="name">Name of the variable.</param>
        /// <returns>True if variable exists, false if not.</returns>
        public bool CheckVariable(String name)
        {
            foreach (String key in VarNameList)
            {
                if (key.Equals(name) && VarNameList.Count != 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Specialised method for initialising method parameters when using a method call.
        /// Only use this when initialising a Method.
        /// It will also sync up the variables between the two before the method is called.
        /// </summary>
        /// <param name="methodParams">The initialised parameters in the method to be added.</param>
        /// <param name="userVariables">The main userVariables.</param>
        /// <remarks>
        /// addVariable is used so that if userVariables and methodVariables has a variable with the same name, methodVariables takes priority.
        /// It is true when a variable should be added, and false if not;
        /// </remarks>
        /// <exception cref="SyntaxException">
        /// Thrown if operation fails.
        /// </exception>
        public void AddMethodParams(List<String> methodParams, UserVariables userVariables)
        {
            int methodParamInt = 0;
            int index = 0;
            foreach (String methodParam in methodParams)
            {
                if (Int32.TryParse(methodParam, out methodParamInt))
                    VarIntList[index] = methodParamInt;
                else
                {
                    if (userVariables.CheckVariable(methodParam))
                        VarIntList[index] = userVariables.GetVariable(methodParam);
                    else
                        throw new SyntaxException($"Error in UserVariables.AddMethodParams: Variable {methodParam} doesn't exist.");
                }
                index++;
            }
            index = 0;
            bool addVariable;
            // This is so the method variables don't overwrite the base ones.
            foreach (String mainVarName in userVariables.VarNameList)
            {
                addVariable = true;
                foreach (String methodVarName in VarNameList)
                {
                    if (methodVarName.Equals(mainVarName))
                        addVariable = false;
                }
                if (addVariable)
                    CreateVariable(mainVarName, userVariables.VarIntList[index]);
                index++;
            }
            return;
        }

        /// <summary>
        /// Used to sync up the UserVariables after a method has been used, 
        /// it will not sync up the variables that are method exclusive.
        /// </summary>
        /// <param name="methodParamNames">The original list of parameters, these should not be carried over to the base variable list.</param>
        /// <param name="methodVariables">The UserVariables class for the method parameters.</param>
        /// <remarks>
        /// updateVariable is used so that if userVariables and methodVariables has a variable with the same name,
        /// the variable in userVariables should not be overwritten by the one in methodVariables.
        /// It is true when a variable should be updated, and false if not;
        /// </remarks>
        public void SyncVariables(List<String> methodParamNames, UserVariables methodVariables)
        {
            bool updateVariable;
            int index = 0;
            foreach (String newMethodVarName in methodParamNames)
            {
                updateVariable = true;
                foreach (String oldMethodVarName in methodVariables.VarNameList)
                {
                    if (newMethodVarName.Equals(oldMethodVarName))
                        updateVariable = false;
                }
                if (updateVariable)
                    EditVariable(newMethodVarName, "=", methodVariables.GetVariable(newMethodVarName));
                index++;
            }
        }

        /// <summary>
        /// Creates an if statement with commands to run later.
        /// </summary>
        /// <param name="commands">Commands to be run through.</param>
        /// <param name="index">Index of the commands where the if statement starts.</param>
        /// <exception cref="SyntaxException">
        /// Thrown if variable name doesn't exist.
        /// Also thrown if user didn't put in an endif.
        /// Also thrown if method is called without a proper if statement.
        /// </exception>
        public void CreateIfStatement(String[] commands, int index)
        {
            if (commands[index].Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries)[0].Equals("If", StringComparison.OrdinalIgnoreCase))
            {
                IfList.Add(null);
                IfConditionList.Add(commands[index].Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries)[1]);
                index++;
                try
                {
                    while (!commands[index].Equals("Endif", StringComparison.OrdinalIgnoreCase))
                    {
                        IfList[ifIndex] += commands[index];
                        IfList[ifIndex] += "\n";
                        index++;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    throw new SyntaxException($"Error: User forgot to add in an endif.");
                }
                ifIndex++;
            }
            else
            {
                throw new SyntaxException($"Error in UserVariables: CreateIfStatement was called without a valid if.");
            }
        }

        /// <summary>
        /// Returns the if statement commands if the condition is satisfied, otherwise returns null.
        /// </summary>
        /// <param name="ifIndex">The index of the if statement array to be gotten.</param>
        /// <returns>The if statement commands if the condition is satisfied, otherwise returns null.</returns>
        public String[] GetIfStatement(int ifIndex)
        {
            String baseIf = null;
            String[] ifStatement = null;

            if (CheckIfStatement(ifIndex))
            {
                baseIf = IfList[ifIndex];
                ifStatement = baseIf.Split(new[] { "\r\n", "\r", "\n" },
                        StringSplitOptions.RemoveEmptyEntries);
                return ifStatement;
            }
            
            // Will return null if condition not satisfied.
            return null;
        }

        /// <summary>
        /// Method that checks if the if statement condition is satisfied.
        /// </summary>
        /// <param name="ifIndex"></param>
        /// <returns>True if condition is satisfied, false if not.</returns>
        /// <exception cref="SyntaxException">
        /// Thrown if variable name doesn't exist.
        /// Also thrown if there if a null reference.
        /// </exception>
        public bool CheckIfStatement(int ifIndex)
        {
            String[] conditions = IfConditionList[ifIndex].Split(seperator1, StringSplitOptions.RemoveEmptyEntries);
            if (conditions.Length == 3)
            {
                if (!Int32.TryParse(conditions[0], out int checkedInt1))
                {
                    if (CheckVariable(conditions[0]))
                        checkedInt1 = GetVariable(conditions[0]);
                    else throw new SyntaxException($"Error in UserVariables.CheckIfStatement: Variable {conditions[0]} doesn't exist.");
                }
                String operatorType = conditions[1];
                if (!Int32.TryParse(conditions[2], out int checkedInt2))
                {
                    if (CheckVariable(conditions[2]))
                        checkedInt2 = GetVariable(conditions[2]);
                    else throw new SyntaxException($"Error in UserVariables.CheckIfStatement: Variable {conditions[2]} doesn't exist.");
                }
                try
                {
                    switch (operatorType)
                    {
                        case "<":
                            if (checkedInt1 < checkedInt2)
                                return true;
                            break;
                        case "=":
                            if (checkedInt1 == checkedInt2)
                                return true;
                            break;
                        case ">":
                            if (checkedInt1 > checkedInt2)
                                return true;
                            break;
                        default:
                            break;
                    }
                }
                catch (NullReferenceException)
                {
                    throw new SyntaxException($"Error in UserVariables.CheckIfStatement: Caught a null reference.");
                }
            }
            return false;
        }

        /// <summary>
        /// Creates a loop with commands to run later.
        /// </summary>
        /// <param name="commands">Commands to be run through.</param>
        /// <param name="index">Index of the commands where the loop starts.</param>
        /// <exception cref="SyntaxException">
        /// Thrown if variable name doesn't exist.
        /// Also thrown if user didn't put in an endloop.
        /// Also thrown if method is called without a proper if statement.
        /// </exception>
        public void CreateLoop(String[] commands, int index)
        {
            if (commands[index].Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries)[0].Equals("Loop", StringComparison.OrdinalIgnoreCase))
            {
                LoopList.Add(null);
                LoopCountList.Add(commands[index].Split(seperator1, StringSplitOptions.RemoveEmptyEntries)[2]);
                index++;
                try
                {
                    while (!commands[index].Equals("Endloop", StringComparison.OrdinalIgnoreCase))
                    {
                        LoopList[loopIndex] += commands[index];
                        LoopList[loopIndex] += "\n";
                        index++;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    throw new SyntaxException($"Error: User forgot to add in an endloop.");
                }
                loopIndex++;
            }
            // This happens if there wasn't a loop after all.
            else
            {
                throw new SyntaxException($"Error in UserVariables: CreateLoop was called without a valid loop.");
            }
        }

        /// <summary>
        /// Returns the loop commands
        /// </summary>
        /// <param name="loopIndex">The index of the loop array to be gotten.</param>
        /// <returns>The loop commands.</returns>
        public String[] GetLoop(int loopIndex)
        {
            String baseLoop = LoopList[loopIndex];
            String[] loopStatement = baseLoop.Split(new[] { "\r\n", "\r", "\n" },
                    StringSplitOptions.RemoveEmptyEntries);
            return loopStatement;
        }

        /// <summary>
        /// Searches the LoopCountList for the number of times the loop should be ran.
        /// </summary>
        /// <param name="loopIndex">The index of the loop array to be gotten.</param>
        /// <returns>The number of times to run the loop.</returns>
        /// <exception cref="SyntaxException">
        /// Thrown if loop number cannot be found
        /// </exception>
        public int GetLoopNumber(int loopIndex)
        {
            if (Int32.TryParse(LoopCountList[loopIndex], out int loopNo))
                return loopNo;
            else if (CheckVariable(LoopCountList[loopIndex]))
            {
                loopNo = GetVariable(LoopCountList[loopIndex]);
                return loopNo;
            }
            throw new SyntaxException("Error in UserVariables.GetLoopNumber: Cannot find loop number.");
        }

        /// <summary>
        /// Checks the name against the invalid names array and also the names of other variables.
        /// </summary>
        /// <param name="name">Variable name</param>
        /// <returns>True if it is an invalid name, false if not.</returns>
        public bool CheckForInvalidName(String name)
        {
            // Method and variable names don't ignore cases.
            foreach (string invalidName in invalidNames)
                if (name.Equals(invalidName, StringComparison.OrdinalIgnoreCase))
                    return true;
            foreach (string varName in VarNameList)
                if (name.Equals(varName))
                    return true;
            return false;
        }

        /// <summary>
        /// Resets the if statements and loops to 0, ready for another run through the program.
        /// </summary>
        public void ResetIfsAndLoops()
        {
            ifIndex = 0;
            IfList.Clear();
            IfConditionList.Clear();
            loopIndex = 0;
            LoopList.Clear();
            LoopCountList.Clear();
        }
    }
}
