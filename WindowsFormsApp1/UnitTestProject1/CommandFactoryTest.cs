﻿using System;
using DrawingProgram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DrawingProgramTest
{
    /// <summary>
    /// Test class for CommandFactory.
    /// </summary>
    [TestClass]
    public class CommandFactoryTest
    {
        /// <summary>
        /// Checks that when the correct command type is input, the correct object is returned.
        /// </summary>
        [TestMethod]
        public void Build_InputCorrectCommand_ReturnsCorrectObject()
        {
            DrawingCommand drawingCommand = CommandFactory.Build("circle");
            Assert.AreEqual(drawingCommand.GetType(), new Circle().GetType());
        }

        /// <summary>
        /// Checks that when the correct command type is input even while case insensitive, the correct object is returned.
        /// </summary>
        [TestMethod]
        public void Build_InputCorrectCommandCaseInsensitive_ReturnsCorrectObject()
        {
            DrawingCommand drawingCommand = CommandFactory.Build("CiRcLe");
            Assert.AreEqual(drawingCommand.GetType(), new Circle().GetType());
        }

        /// <summary>
        /// Checks that when an incorrect command type is input, an exception is thrown.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void Build_InputIncorrectCommand_ThrowsException()
        {
            try
            {
                DrawingCommand drawingCommand = CommandFactory.Build("cycle");
            }
            catch (Exception) { Assert.Fail(); }
        }
    }
}
