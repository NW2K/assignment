﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{
    /// <summary>
    /// Drawing class that draws a straight line from one point to another on a bitmap.
    /// </summary>
    public class DrawTo : DrawingCommand
    {
        /// <summary>
        /// New x position.
        /// </summary>
        public int newX { get; private set; }

        /// <summary>
        /// New y position.
        /// </summary>
        public int newY { get; private set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DrawTo()
        {
            SetPosition(0, 0);
            newX = 0;
            newY = 0;
        }

        /// <summary>
        /// Constructor where the x and y values are put in
        /// </summary>
        /// <param name="x">The x value of point 1.</param>
        /// <param name="y">The y value of point 1.</param>
        /// <param name="addX">The value to be added onto x to make point 2.</param>
        /// <param name="addY">The value to be added onto y to make point 2.</param>
        public DrawTo(int x, int y, int addX, int addY)
        {
            SetPosition(x, y);
            this.newX = x + addX;
            this.newY = y + addY;
        }

        /// <summary>
        /// Method used to change the x, y, newX and newY values after construction.
        /// </summary>
        /// <param name="x">The x value of point 1.</param>
        /// <param name="y">The y value of point 1.</param>
        /// <param name="addX">The value to be added onto x to make point 2.</param>
        /// <param name="addY">The value to be added onto y to make point 2.</param>
        /// <param name="dummy">Dummy variable.</param>
        public override void SetValues(int x, int y, int addX, int addY, int dummy)
        {
            SetPosition(x, y);
            this.newX = x + addX;
            this.newY = y + addY;
        }

        /// <summary>
        /// Runs the program and draws a line.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        public override void RunCommand(Bitmap myBitmap)
        {
            Point pt1 = new Point(X, Y);
            Point pt2 = new Point(newX, newY);
            Pen myPen = new Pen(Color.Black, 5);

            Graphics g = Graphics.FromImage(myBitmap);
            g.DrawLine(myPen, pt1, pt2);
            myPen.Dispose();
        }

        
    }
}
