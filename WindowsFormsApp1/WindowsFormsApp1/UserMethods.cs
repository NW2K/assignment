﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{
    /// <summary>
    /// Class that contains the user's methods.
    /// </summary>
    public class UserMethods
    {
        /// <summary>
        /// List of methods.
        /// </summary>
        public List<String> MethodList { get; private set; } = new List<String>();
        /// <summary>
        /// List of method names.
        /// </summary>
        public List<String> MethodNameList { get; private set; } = new List<String>();
        /// <summary>
        /// List of method variables.
        /// </summary>
        public List<UserVariables> methodVariables { get; private set; } = new List<UserVariables>();
        /// <summary>
        /// List of flags on whether a method uses parameters or not.
        /// </summary>
        public List<bool> methodUsesParameters { get; private set; } = new List<bool>();
        /// <summary>
        /// Initial list of parameters used to create and call the method. Used in syntax checking.
        /// </summary>
        public List<List<String>> initialMethodParams { get; private set; } = new List<List<String>>();

        /// <summary>
        /// Index of the method array.
        /// </summary>
        private int methodCount = 0;
      
        // Seperator with spaces.
        private readonly String[] seperator1 = { " " };

        /// <summary>
        /// String of invalid names for methods, these include all types of commands to mitigate confusion.
        /// </summary>
        private String[] invalidNames = { "MoveTo", "DrawTo", "Rectangle", "Circle", "CircleFill",
            "Triangle", "Polygon", "Clear", "Reset", "Save", "Load", "Help", "If", "Loop",
            "Method", "Var", "Call", "Run" };

        /// <summary>
        /// Create a method that does not take in parameters.
        /// </summary>
        /// <param name="name">Name of the method.</param>
        /// <param name="commands">Commands to be run through.</param>
        /// <param name="index">Index of the commands where the method starts.</param>
        /// <exception cref="SyntaxException">
        /// Thrown if method name is invalid.
        /// Also thrown if user didn't put in an endmethod.
        /// Also thrown if method is called without a proper method.
        /// </exception>
        public void CreateMethod(String name, String[] commands, int index)
        {
            if (CheckForInvalidName(name))
                throw new SyntaxException($"Error in method name ({name}): Name already exists.");


            if (commands[index].Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries)[0].Equals("Method", StringComparison.OrdinalIgnoreCase))
            {
                MethodNameList.Add(name);
                methodVariables.Add(null);
                initialMethodParams.Add(null);
                methodUsesParameters.Add(false);
                MethodList.Add(null);
                index++;
                try
                {
                    while (!commands[index].Equals("Endmethod", StringComparison.OrdinalIgnoreCase))
                    {
                        MethodList[methodCount] += commands[index];
                        MethodList[methodCount] += "\n";
                        index++;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    throw new SyntaxException($"Error: User forgot to add in an endmethod.");
                }
                methodCount++;
            }
            // This happens if there wasn't a method after all.
            else
            {
                throw new SyntaxException($"Error in UserMethods: CreateMethod was called without a valid method.");
            }
        }

        /// <summary>
        /// Create a method that can take in parameters.
        /// </summary>
        /// <param name="name">Name of the method.</param>
        /// <param name="parameterNames">Names of the parameters.</param>
        /// <param name="commands">Commands to be run through.</param>
        /// <param name="index">Index of the commands where the method starts.</param>
        /// <exception cref="SyntaxException">
        /// Thrown if method name is invalid.
        /// Also thrown if user didn't put in an endmethod.
        /// Also thrown if method is called without a proper method.
        /// </exception>
        public void CreateMethod(String name, List<String> parameterNames, String[] commands, int index)
        {
            if (CheckForInvalidName(name))
                throw new SyntaxException($"Error in variable {name}: Invalid parameter name.");

            if (commands[index].Split(seperator1, 2, StringSplitOptions.RemoveEmptyEntries)[0].Equals("Method", StringComparison.OrdinalIgnoreCase))
            {
                MethodNameList.Add(name);
                methodVariables.Add(new UserVariables());
                initialMethodParams.Add(parameterNames);
                foreach (String parameterName in parameterNames)
                {
                    methodVariables[methodCount].CreateVariable(parameterName, 0);
                }
                methodUsesParameters.Add(true);
                MethodList.Add(null);
                index++;
                try
                {
                    while (!commands[index].Equals("Endmethod", StringComparison.OrdinalIgnoreCase))
                    {
                        MethodList[methodCount] += commands[index];
                        MethodList[methodCount] += "\n";
                        index++;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    throw new SyntaxException($"Error: User forgot to add in an endmethod.");
                }
                methodCount++;
            }
            // This happens if there wasn't a method after all.
            else
            {
                throw new SyntaxException($"Error in UserMethods: CreateMethod was called without a valid method.");
            }
        }

        /// <summary>
        /// Returns the method commands
        /// </summary>
        /// <param name="name">Name of the method</param>
        /// <returns>The method commands.</returns>
        /// <exception cref="SyntaxException">
        /// Thrown if method name doesn't exist.
        /// </exception>
        public String[] GetMethod(String name)
        {
            int index = 0;
            String baseMethod = null;
            String[] method = null;
            foreach (String key in MethodNameList)
            {
                if (key.Equals(name) && methodCount != 0)
                {
                    baseMethod = MethodList[index];
                    method = baseMethod.Split(new[] { "\r\n", "\r", "\n" },
                        StringSplitOptions.RemoveEmptyEntries);
                    return method;
                }
                index++;
            }
            throw new SyntaxException($"Error in UserMethods: Method {name} doesn't exist.");
        }

        /// <summary>
        /// Returns the method UserVariables object.
        /// </summary>
        /// <param name="name">Name of the method.</param>
        /// <returns>The method UserVariables object.</returns>
        /// <exception cref="SyntaxException">
        /// Thrown if method name doesn't exist.
        /// </exception>
        public UserVariables GetMethodVariables(String name)
        {
            int index = 0;
            foreach (String key in MethodNameList)
            {
                if (key.Equals(name) && methodCount != 0)
                {
                    return methodVariables[index];
                }
                index++;
            }
            throw new SyntaxException($"Error in UserMethods: Method {name} doesn't exist.");
        }

        /// <summary>
        /// Returns the initial method parameter names used in the construction of this method.
        /// </summary>
        /// <param name="name">Name of the method.</param>
        /// <returns>Initial method parameter names.</returns>
        /// <exception cref="SyntaxException">
        /// Thrown if method name doesn't exist.
        /// </exception>
        public List<String> GetInitialMethodParams(String name)
        {
            int index = 0;
            foreach (String key in MethodNameList)
            {
                if (key.Equals(name) && methodCount != 0)
                {
                    return initialMethodParams[index];
                }
                index++;
            }
            throw new SyntaxException($"Error in UserMethods: Method {name} doesn't exist.");
        }

        /// <summary>
        /// Checks if a particular method exists.
        /// </summary>
        /// <param name="name">Name of the method.</param>
        /// <returns>True if method exists, false if not.</returns>
        public bool CheckMethod(String name)
        {
            foreach (String key in MethodNameList)
            {
                if (key.Equals(name) && methodCount != 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks the name against the invalid names array and also the names of other methods.
        /// </summary>
        /// <param name="name">Method name.</param>
        /// <returns>True if it is an invalid name, false if not.</returns>
        public bool CheckForInvalidName(String name)
        {
            // Method and variable names don't ignore cases.
            foreach (string invalidName in invalidNames)
                if (name.Equals(invalidName, StringComparison.OrdinalIgnoreCase))
                {
                    throw new SyntaxException("Error: Method name is invalid.");
                }
            foreach (string methodName in MethodNameList)
                // Maybe case insensitive?
                if (name.Equals(methodName))
                {
                    throw new SyntaxException($"Error: Method name {methodName} already exists.");
                }
            return false;
        }
    }
}
