﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingProgram
{
    /// <summary>
    /// Factory class that returns objects that inherit DrawingCommand.
    /// </summary>
    public static class CommandFactory
    {
        /// <summary>
        /// Returns an appropriate DrawingCommand object depending on command.
        /// </summary>
        /// <param name="command">Command type for the object.</param>
        /// <returns>One of the DrawingCommand objects.</returns>
        /// <exception cref="SyntaxException">
        /// Thrown if command did not fall under any of the cases.
        /// </exception>
        public static DrawingCommand Build(String command)
        {
            /*
             * When adding a new drawing command to the program, one must:
             * 1. Add a class that inherits DrawingCommand.
             * 2. Add the name of the class to this case switch
             * 3. Add the name of the class the basicCommands in CommandChecker.
             * 4. Add the name of the class to the list of invalid variables names in UserVariables.
             * 5. Add the name of the class to the list of invalid method names in UserMethods.
             */
            switch (command.ToLower())
            {
                case "moveto":
                    return new MoveTo();
                case "drawto":
                    return new DrawTo();
                case "rectangle":
                    return new Rectangle();
                case "circle":
                    return new Circle();
                case "triangle":
                    return new Triangle();
                case "circlefill":
                    return new CircleFill();
                default:
                    throw new SyntaxException($"Error in {command}: Command doesn't fall under any of the valid commands.");
            }
        }
    }
}
