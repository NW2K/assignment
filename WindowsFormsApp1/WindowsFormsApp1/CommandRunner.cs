﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProgram
{
    /// <summary>
    /// Class that interacts with the Bitmap using DrawingCommand classes.
    /// </summary>
    public class CommandRunner
    {
        /// <summary>
        /// Current x position of the pen.
        /// </summary>
        public int XPosition { get; private set; }

        /// <summary>
        /// Current y position of the pen.
        /// </summary>
        public int YPosition { get; private set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CommandRunner()
        {

        }

        /// <summary>
        /// Constructor where initial x and y values are put in.
        /// </summary>
        /// <param name="xValue">Initial x position.</param>
        /// <param name="yValue">Initial y position.</param>
        public CommandRunner(int xValue, int yValue)
        {
            XPosition = xValue;
            YPosition = yValue;
        }

        /// <summary>
        /// Runs a command which takes in additional parameters, the parameters used depend on the command type, and some parameters may be dummied.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        /// <param name="command">The command entered in.</param>
        /// <param name="param1">1st Parameter.</param>
        /// <param name="param2">2nd Parameter.</param>
        /// <param name="param3">3rd Parameter.</param>
        /// <remarks>
        /// If the command if either DrawTo or MoveTo, program needs to update the position reference in this class.
        /// </remarks>
        public void RunBasicCommand(Bitmap myBitmap, string command, int param1, int param2, int param3)
        {
            var drawingCommand = CommandFactory.Build(command);
            drawingCommand.SetValues(XPosition, YPosition, param1, param2, param3);
            drawingCommand.RunCommand(myBitmap);
            if (drawingCommand.GetType().Equals("DrawTo") || Equals("MoveTo"))
            {
                UpdatePosition(param1, param2);
            }
        }

        /// <summary>
        /// Runs a command which has no additional parameters.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        /// <param name="command">The command passed down.</param>
        /// <exception cref="SyntaxException">
        /// Thrown if command is invalid.
        /// </exception>
        public void RunSingleCommand(Bitmap myBitmap, String command)
        {
            if (command.Equals("Clear", StringComparison.OrdinalIgnoreCase))
            {
                ClearBitmap(myBitmap);
            }
            else if (command.Equals("Reset", StringComparison.OrdinalIgnoreCase))
            {
                ResetPosition();
            }
            else
            {
                throw new SyntaxException($"Error in CommandRunner.RunSingleCommand: {command} is an invalid single command.");
            }
        }

        /// <summary>
        /// Method to change the x and y positions.
        /// </summary>
        /// <param name="param1">The addition to the x position.</param>
        /// <param name="param2">The addition to the y position.</param>
        private void UpdatePosition(int param1, int param2)
        {
            XPosition += param1;
            YPosition += param2;
        }

        /// <summary>
        /// Clears the bitmap.
        /// </summary>
        /// <param name="myBitmap">The bitmap passed down.</param>
        private void ClearBitmap(Bitmap myBitmap)
        {
            Graphics g = Graphics.FromImage(myBitmap);
            g.Clear(Color.Empty);
        }

        /// <summary>
        /// Resets the x and y positions back to 0.
        /// </summary>
        private void ResetPosition()
        {
            XPosition = 0;
            YPosition = 0;
        }
    }
}
