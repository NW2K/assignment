﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingProgram
{
    /// <summary>
    /// Class that is used to save a file.
    /// </summary>
    public class SaveFile
    {
        /// <summary>
        /// Main dialog for saving a file.
        /// </summary>
        private SaveFileDialog saveFileDialog = new SaveFileDialog();

        /// <summary>
        /// Method used to save a program as a text file.
        /// </summary>
        /// <param name="commands">Program to be saved.</param>
        public void Save(string[] commands)
        {
            saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "Save an Text File";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                System.IO.File.WriteAllLines(saveFileDialog.FileName, commands);
                MessageBox.Show("Saved program as " + saveFileDialog.FileName);
            }
        }

        /// <summary>
        /// Method used to save a program as a image (default is gif).
        /// </summary>
        /// <param name="myBitmap">Image to be saved.</param>
        public void Save(Bitmap myBitmap)
        {
            saveFileDialog.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            saveFileDialog.Title = "Save an Image File";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                try
                {
                    myBitmap.Save(saveFileDialog.FileName);
                    MessageBox.Show("Saved picture as " + saveFileDialog.FileName);
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("Error", "Cannot find file");
                }
                catch (IOException)
                {
                    MessageBox.Show("Error", "IO exception");
                }

            }
        }
    }
}
